﻿using Practice.HomeWork.Abstractions;
using Practice.HomeWork.Abstractions.BLL;
using System.Collections.Generic;
using System.Linq;

namespace Practice.HomeWork.Services
{
    public class UserStore : IUserStore
    {
        private readonly IDatabaseService dbService = new InMemoryDatabaseService();

        public IEnumerable<IUser> Users => this.dbService.Read<IUser>();

        public void AddUser(IUser user)
        {
            this.dbService.Write(user);
        }

        public IUser FindUser(string name)
        {
            return this.dbService.Read<IUser>()
                .FirstOrDefault(user => user.Name == name);
        }

        public IUser FindUser(int id)
        {
            return this.dbService.Read<IUser>()
                .FirstOrDefault(user => user.Id == id);
        }
    }
}