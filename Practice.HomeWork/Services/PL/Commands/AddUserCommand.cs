﻿using System;
using Practice.HomeWork.Abstractions;
using Practice.HomeWork.Abstractions.BLL;
using Practice.HomeWork.Models;

namespace Practice.HomeWork.Services.PL.Commands
{
    public class AddUserCommand : ICommand
    {
        private readonly IUserStore userStore = new UserStore();

        public int Number { get; } = 1;
        public string DisplayName { get; } = "Add user";

        public void Execute()
        {
            var rnd = new Random();
            var id = rnd.Next(1, 101);

            this.userStore.AddUser(new User
            {
                Id = id,
                Name = $"User {id}"
            });
        }
    }
}