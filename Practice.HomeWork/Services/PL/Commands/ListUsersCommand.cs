﻿using Practice.HomeWork.Abstractions;
using Practice.HomeWork.Abstractions.BLL;
using System;

namespace Practice.HomeWork.Services.PL.Commands
{
    public class ListUsersCommand : ICommand
    {
        private readonly IUserStore userStore = new UserStore();

        public int Number { get; } = 2;

        public string DisplayName { get; } = "List users";

        public void Execute()
        {
            var users = this.userStore.Users;

            foreach (var user in users)
            {
                Console.WriteLine($"{user.Id}. {user.Name}");
            }
        }
    }
}