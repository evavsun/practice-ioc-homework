﻿using Practice.HomeWork.Services.PL;

namespace Practice.HomeWork
{
    internal class Program
    {
        private static void Main()
        {
            // Your Inversion of Control (Simple Injector)

            var manager = new CommandManager();

            manager.Start();
        }
    }
}
