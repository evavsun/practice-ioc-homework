﻿using Practice.HomeWork.Abstractions.BLL;

namespace Practice.HomeWork.Models
{
    public class User : IUser
    {
        public int Id { get; internal set; }

        public string Name { get; set; }
    }
}