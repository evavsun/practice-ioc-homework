﻿using System.Collections.Generic;

namespace Practice.HomeWork.Abstractions
{
    public interface ICommandProcessor
    {
        void Process(int command);

        IEnumerable<ICommand> Commands { get; }
    }
}