﻿namespace Practice.HomeWork.Abstractions
{
    public interface ICommand
    {
        int Number { get; }

        string DisplayName { get; }

        void Execute();
    }
}