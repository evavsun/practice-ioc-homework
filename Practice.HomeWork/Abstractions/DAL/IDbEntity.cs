﻿namespace Practice.HomeWork.Abstractions
{
    public interface IDbEntity
    {
        int Id { get; }
    }
}