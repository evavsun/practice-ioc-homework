﻿namespace Practice.HomeWork.Abstractions.BLL
{
    public interface IUser : IDbEntity
    {
        string Name { get; set; }
    }
}