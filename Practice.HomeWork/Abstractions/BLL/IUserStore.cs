﻿using System.Collections.Generic;

namespace Practice.HomeWork.Abstractions.BLL
{
    public interface IUserStore
    {
        IEnumerable<IUser> Users { get; }

        void AddUser(IUser user);

        IUser FindUser(string name);

        IUser FindUser(int id);
    }
}